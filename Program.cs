﻿using System;

namespace Shapes
{
    class Program
    {
        static void Main(string[] args)
        {
            var size = 14; // your possible input arg

            var value = " " + (args.Length != 1 || String.IsNullOrWhiteSpace(args[0]) || args[0].Length > 1 ? "*" : args[0].Trim());

            // if (size < 2) return;
            // For what purpose this statement exist? 

            var line = "";
            for (int i = 0; i < size; i++)
            {
                line += value;
            }

            var body = value + (new String(' ', size * 2 - 4)) + value; 
            // We always use by 2 symbols from right and left sides, why 2 * 2 instead of just 4?

            Console.WriteLine(line);

            if (size >= 3)
            {
                for (int i = 0; i < size - 2; i++)
                {
                    Console.WriteLine(body);
                }
            }

            Console.WriteLine(line);
            
            Console.WriteLine(); // Space between shapes

            var dot = (new String(' ', size - 1)) + value;

            Console.WriteLine(dot);

            if (size >= 3)
            {
                // Top part
                for (int i = 0; i < (size - 2) / 2; i++)
                {
                    body = (new String(' ', size - 2 - i)) + value + (new String(' ', i * 2)) + value;
                    Console.WriteLine(body);
                }
            
                // Bottom part
                for (int i = 0; i < (size - 2) / 2; i++)
                {
                    body = (new String(' ', (size / 2) + i)) + value + (new String(' ', size - 4 - (i * 2))) + value;
                    Console.WriteLine(body);
                }
            }

            Console.WriteLine(dot);

        }
    }
}
